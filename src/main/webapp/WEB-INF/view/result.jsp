<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Analizues Teksti</title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            color: #ffffff;
            background-color: #9954bb;
            border: 1px solid #723b8b;
            border-bottom-color: transparent;
        }
        #rrokjet{
            word-spacing: 10px;
        }
    </style>

</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand">AT</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="/">Ballina</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container" style="margin-top:20px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Analizues Teksti
                    <small>Analizë e thjeshtë e tekstit</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h3>Teksti për analizë</h3>
            <hr>
            <div class="well">
                <form action="analyzeText" method="POST">
                    <div class="form-group">
                        <label for="text">Teksti:</label>
                        <button class="btn btn-success btn-xs pull-right">Analizo</button>
                        <br>
                        <textarea name="text" class="form-control" id="text" rows="30"
                                  placeholder="Shkruani tekstin për analizë...">${text}</textarea>
                    </div>
                </form>

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#lidhzat" data-toggle="tab" aria-expanded="true">Lidhëzat</a></li>
                        <li><a href="#rrokjet" data-toggle="tab" aria-expanded="false">Rrokjet</a></li>
                        <li class="disabled"><a>Kryefjalët</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                Mbiemrat <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="disabled"><a>Gjini</a></li>
                                <li class="disabled"><a>Numër</a></li>
                                <li class="disabled"><a>Rasë</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="lidhzat">
                            <c:forEach items="${lidhezat}" var="kategoriaLidhzes">
                                <h3 style="padding:10px 18px; color: #ffffff;background-color: #222222;border-color: #222222;">${kategoriaLidhzes.key}</h3>
                                <blockquote>
                                    <table class="table table-striped table-hover">
                                        <tbody>
                                        <c:set var="count" value="0" scope="page"/>
                                        <c:set var="totalCount" value="0" scope="page"/>
                                        <c:forEach items="${kategoriaLidhzes.value}" var="lidheza">
                                            <tr class="<c:if test="${lidheza.value > 0}">success</c:if> ">
                                                <td class="col-xs-9"><strong>${lidheza.key}</strong></td>
                                                <td class="col-xs-3">${lidheza.value}</td>
                                            </tr>
                                            <c:set var="count" value="${count + 1}" scope="page"/>
                                            <c:set var="totalCount" value="${totalCount + lidheza.value}" scope="page"/>
                                        </c:forEach>
                                        </tbody>

                                    </table>

                                    <em>
                                        Nga <strong>${count}</strong>
                                        <c:if test="${count == 0}">lidhëz</c:if>
                                        <c:if test="${count > 0}">lidhëza</c:if>
                                        „${kategoriaLidhzes.key}”
                                        <strong>${totalCount}</strong> rastisje.
                                    </em>
                                </blockquote>
                            </c:forEach>
                        </div>
                        <div class="tab-pane fade" id="rrokjet">
                            <c:forEach items="${cleanText}" var="fjala">
                                <c:forEach items="${rrokjet.get(fjala)}" var="rrokja" varStatus="loop">${rrokja}<c:if test="${!loop.last}">-</c:if></c:forEach>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="collapse" id="lidhezatCollapse">

                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <h3>Rezultati</h3>
            <hr>
            <div class="row">

                <div class="col-md-4" style="padding-right: 0;">
                    <ul class="list-group">

                        <li class="list-group-item"><strong>Shenjat</strong></li>
                        <c:if test="${extra.size() >0}">
                            <c:forEach items="${extra}" var="karakteri">
                                <li class="list-group-item">
                                    <c:choose>
                                        <c:when test='${karakteri.key == " "}'><em>
                                            <small>hapësirë</small>
                                        </em></c:when>
                                        <c:when test='${karakteri.key == ("&#13;&#10;")}'><em>
                                            <small>rresht i ri</small>
                                        </em></c:when>
                                        <c:otherwise>${karakteri.key}</c:otherwise>
                                    </c:choose>
                                    <span class="badge">${karakteri.value}</span>
                                </li>
                            </c:forEach>
                        </c:if>
                    </ul>
                    <ul class="list-group">

                        <li class="list-group-item"><strong>Numrat</strong></li>
                        <c:if test="${numrat.size() >0}">
                            <c:forEach items="${numrat}" var="numri">
                                <li class="list-group-item">
                                        ${numri.key} <span class="badge">${numri.value}</span>
                                </li>
                            </c:forEach>
                        </c:if>
                    </ul>
                </div>


                <div class="col-md-3" style="padding-right: 0;">
                    <ul class="list-group">

                        <li class="list-group-item"><strong>Shkronjat</strong></li>
                        <c:if test="${totalShkronjat > 0 }">
                            <c:forEach items="${shkronjat}" var="karakteri">
                                <li class="list-group-item">
                                        ${karakteri.key}
                                    <span class="badge">${karakteri.value}</span>
                                </li>
                            </c:forEach>
                        </c:if>
                    </ul>
                </div>


                <div class="col-md-5">
                    <ul class="list-group">
                        <li class="list-group-item"><strong>Frekuenca e përdorimit</strong></li>
                        <c:if test="${totalShkronjat > 0 }">
                            <c:forEach items="${shkronjat}" var="karakteri">
                                <li class="list-group-item">
                                        ${karakteri.key}
                                    <span class="badge">
                                        <c:choose>
                                            <c:when test="${totalShkronjat>0}">
                                                <fmt:formatNumber maxFractionDigits="10" type="number" groupingUsed="false"
                                                                  value="${karakteri.value/totalShkronjat}"/>
                                            </c:when>
                                            <c:otherwise>
                                                0
                                            </c:otherwise>
                                        </c:choose>
                                    </span>
                                </li>
                            </c:forEach>
                        </c:if>
                    </ul>
                </div>


            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-group">
                        <li class="list-group-item"><strong>Përmbledhje</strong></li>
                        <li class="list-group-item">Gjatësia e tekstit
                            <span class="badge">${gjatesiaTotale}</span>
                        </li>
                        <li class="list-group-item">
                            Gjithsej shkronja
                            <span class="badge">${totalShkronjat}</span>
                        </li>
                        <li class="list-group-item">Gjithsej shenja
                            <span class="badge">${totalExtra}</span>
                        </li>
                        <li class="list-group-item">Gjithsej numra
                            <span class="badge">${totalNumrat}</span>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    </div>
    <div class="row">
        <div class="col-md-12" style="position:relative; height: 410px;">
            <canvas id="myChart"></canvas>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<c:url value="/resources/js/jquery-3.2.1.min.js" />"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

<script src="<c:url value="/resources/js/Chart.min.js" />"></script>
<script type="text/javascript">

    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        responsive: true,
        maintainAspectRatio: false,
        data: {
            labels: [<c:forEach items="${shkronjatKeys}" var="strChar"> "${strChar}", </c:forEach>],
            datasets: [
                {
                    label: "Shkronjat",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,1)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [<c:forEach items="${shkronjatValuesArray}" var="strVlera">${strVlera}, </c:forEach>],
                    spanGaps: false,
                },
                {
                    label: "Frekuenca e shkronjave",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(192,75,192,0.4)",
                    borderColor: "rgba(192,75,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(192,75,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(192,75,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [<c:forEach items="${shkronjatValuesArray}" var="strVlera"><fmt:formatNumber maxFractionDigits="6" type="number" groupingUsed="false" value="${strVlera/totalShkronjat}" />, </c:forEach>],
                    spanGaps: false,
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>
</body>
</html>