package com.blendigashi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.ModelMap;

import java.util.*;


@Controller
public class HomeController {

    @RequestMapping("/")
    public String home() {
        return "ballina";
    }

    @RequestMapping(value = "/analyzeText", method = RequestMethod.POST)
    public String analyzeText(@RequestParam String text, Model model) {

        TextAnalyzer ta = new TextAnalyzer(text, "sq");

        model.addAttribute("shkronjatKeys", ta.getShkronjatKeys());
        model.addAttribute("shkronjat", ta.getShkronjat());
        model.addAttribute("shkronjatValuesArray", ta.getShkronjatValues());
        model.addAttribute("totalShkronjat", ta.getTotalShkronjat());

        model.addAttribute("numrat", ta.getNumrat());
        model.addAttribute("totalNumrat", ta.getTotalNumrat());

        model.addAttribute("extra", ta.getExtra());
        model.addAttribute("totalExtra", ta.getTotalExtra());

        model.addAttribute("lidhezat", ta.getLidhezat());
        model.addAttribute("rrokjet", ta.getRrokjet());
        model.addAttribute("cleanText", ta.getCleanText().split(" "));

        model.addAttribute("gjatesiaTotale", ta.getGjatesiaTotale());
        model.addAttribute("text", text);
        return "result";
    }
}
