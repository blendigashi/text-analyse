package com.blendigashi.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * Created by Blendi Gashi on 4/11/2017.
 */
class Rrokja {
    private String text;
    private String fjala;
    private LinkedHashMap<String, ArrayList<String>> fjaletMeRrokje;
    private GjuhaShqipe sq;

    ArrayList<String> rrokjet;


    Rrokja(String t, GjuhaShqipe sq) {
        this.text = t;
        this.sq = sq;

        fjaletMeRrokje = new LinkedHashMap<String, ArrayList<String>>();


        for (String tmp : this.text.split(" ")) {
            this.fjala = tmp;
            ArrayList<String> tmpRrokje = null;
            if(sq.eshteFjaleTjeter(this.fjala)){
                tmpRrokje = sq.merrRrokjet(this.fjala);
            }
            else{
                tmpRrokje = gjejRrokjet();
            }
            fjaletMeRrokje.put(tmp, tmpRrokje);

        }
    }

    LinkedHashMap<String, ArrayList<String>> getRrokjet() {
        return this.fjaletMeRrokje;
    }

    private ArrayList<String> gjejRrokjet() {

        rrokjet = new ArrayList<String>();

        int mbetja = 0; // Percjell ndryshimet e fjales gjate ndarjes ne rrokje. Mban numrin e ndryshimit te fundit

        for (int i = 0; i < this.fjala.length(); i++) {

            if (sq.eshteZanore(this.fjala.charAt(i))) { // Nese kemi hasur nje zanore

                int j = gjejZanorenTjeter(i); // Tregon indeksin e zanores tjetër

                if (j != -1) { //Nese ka ndonje zanore tjeter ne fjale kontrollo rrokjet

                    /*
                    Llogarit bashtingelloret ne mes zanoreve
                     */
                    int distancaMesZanoreve = llogaritB(i, j);

                    /*
                    Nese ska mbashtingellore fute ate si rrokje dhe vazhdo me zanoren tjeter
                     */
                    if (distancaMesZanoreve == 0 || distancaMesZanoreve == 1) {
                        shtoRrokjen(mbetja, i);
                        mbetja = i + 1;
                    } else if (distancaMesZanoreve == 2) {
                        String b1 = "";
                        String b2 = "";
                        /*
                        Kontrollojme nese kemi hasur ne diftogjne, sepse ndarja behet ndryshe
                         */
                        String tmpString = this.fjala.charAt(i + 1) + "" + this.fjala.charAt(i + 2);
                        if (sq.eshteDiftongje(tmpString)) { // Nese e para eshte diftongje
                            b1 = tmpString;
                            b2 = String.valueOf(this.fjala.charAt(i + 3));
                            /*tmpString = this.fjala.charAt(i + 3) + "" + this.fjala.charAt(i + 4);
                            if(sq.eshteDiftongje(tmpString)){ // Nese edhe e dyta eshte diftongje
                                b2 = tmpString;
                            }
                            else
                                b2 = String.valueOf(this.fjala.charAt(i + 3));*/
                        } else { //Nese e para nuk eshte diftongje
                            b1 = String.valueOf(this.fjala.charAt(i + 1));
                            b2 = String.valueOf(this.fjala.charAt(i + 2));

                            /*tmpString = this.fjala.charAt(i + 2) + "" + this.fjala.charAt(i + 3);
                            if (sq.eshteDiftongje(tmpString)) { // Nese e dyta eshte diftongje
                                b2 = tmpString;
                            } else
                                b2 = String.valueOf(this.fjala.charAt(i + 2));*/
                        }
                        if (firstCase(b1, b2)) {
                            if (sq.eshteDiftongje(b1)) {
                                shtoRrokjen(mbetja, i, 2);
                                mbetja = i + 3;
                            } else {
                                shtoRrokjen(mbetja, i, 1);
                                mbetja = i + 2;
                            }
                        } else {
                            shtoRrokjen(mbetja, i);
                            mbetja = i + 1;
                        }
                    } else if (distancaMesZanoreve == 3) {
                        shtoRrokjen(mbetja, i);
                        mbetja = i + 1;
                    } else {
                        shtoRrokjen(mbetja, i, 1);
                        mbetja = i + 2;
                    }

                } else {
                    /*
                    Nese ska zanore tjeter, fute komplet pjesen e mbetur te fjalise
                     */
                    shtoRrokjen(mbetja, this.fjala.length(), -1);
                }
            }
        }
        return rrokjet;
    }

    /*
    Kthen numrin e bashtingelloreve ndermjet dy zanoreve
     */
    private int llogaritB(int start, int end) {
        int tmp = 0;
        for (int i = start + 1; i < end; i++) {
            String tmpString = this.fjala.charAt(i) + "" + this.fjala.charAt(i + 1);
            if (sq.eshteDiftongje(tmpString)) {
                tmp++;
                i++;
            } else {
                tmp++;
            }
        }
        return tmp;
    }

    private int gjejZanorenTjeter(int i) {

        for (i++; i < this.fjala.length(); i++) { // Vazhdojme kerkimin nga nje elemnt me pas
            if (sq.eshteZanore(this.fjala.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    private boolean firstCase(String b1, String b2) {
        if ("jrrll".contains(b1) && "bcçdhfgjhkpqshtthvxhzh".contains(b2)) {
            return true;
        } else if ("jrrllmnj".contains(b1) && "jrrllmnj".contains(b2)) {
            return true;
        }
        return false;
    }

    /*
    @param start Fillimi i rrokjes
    @param end Fundi i rrokjes
    @param skip Karaktere shtesë
     */
    private void shtoRrokjen(int start, int end) {

        rrokjet.add(this.fjala.substring(start, end + 1));
    }

    private void shtoRrokjen(int start, int end, int skip) {

        rrokjet.add(this.fjala.substring(start, end + 1 + skip));
    }

}
