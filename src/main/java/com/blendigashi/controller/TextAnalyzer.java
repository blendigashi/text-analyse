package com.blendigashi.controller;

import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by Blendi Gashi on 4/4/2017.
 */
public class TextAnalyzer {
    private String text;
    private Map<String, Integer> shkronjat;
    private Map<String, Integer> numrat;
    private Map<String, Integer> extra;
    private Rrokja rrokja;
    private Lidheza lidheza;
    private GjuhaShqipe sq;

    private Map<String, LinkedList<String>> rrokjet;

    private int gjatesiaTotale;

    TextAnalyzer(String text, String sq) {
        if(sq.equals("sq")){
            this.sq = new GjuhaShqipe();
        }
        else
            this.sq = new GjuhaShqipe();

        this.text = text.toLowerCase(Locale.forLanguageTag("al"));

        this.gjatesiaTotale = this.text.length();

        /*
        Per tekstin e dhene kryen analizen
         */
        analyze();
        /*
        Nxjerrja e lidhesave
         */

        lidheza = new Lidheza(this.text);

        /*
        Nxjerrja e rrokjeve
         */

        rrokja = new Rrokja(getCleanText(), this.sq);
    }

    int getGjatesiaTotale() {
        return this.gjatesiaTotale;
    }

    int getTotalShkronjat() {
        int tmp = 0;
        for (String k : this.shkronjat.keySet()) {
            tmp += this.shkronjat.get(k);
        }
        return tmp;
    }

    int getTotalNumrat() {
        int tmp = 0;
        for (String k : this.numrat.keySet()) {
            tmp += this.numrat.get(k);
        }
        return tmp;
    }

    int getTotalExtra() {
        int tmp = 0;
        for (String k : this.extra.keySet()) {
            tmp += this.extra.get(k);
        }
        return tmp;
    }

    String getCleanText(){
        return this.text.replaceAll("[^A-Za-zëwç \\t\\n\\r]*", "");
    }

    LinkedHashMap<String, Integer> getShkronjat() {
        return (LinkedHashMap<String, Integer>) this.shkronjat;
    }

    LinkedHashMap<String, Integer> getNumrat() {
        return (LinkedHashMap<String, Integer>) this.numrat;
    }

    LinkedHashMap<String, Integer> getExtra() {
        return (LinkedHashMap<String, Integer>) this.extra;
    }


    Integer[] getShkronjatValues() {
        return this.shkronjat.values().toArray(new Integer[this.shkronjat.size()]);
    }

    String[] getShkronjatKeys() {
        return this.shkronjat.keySet().toArray(new String[shkronjat.size()]);
    }


    /*
    Kthen indeksin ku me vazhdu me iterimin
     */
    private int kontrolloGrupshkronjat(int i) {
        String tmp = text.charAt(i)+""+text.charAt(i+1);
        if(sq.eshteDiftongje(tmp)){
            i++;
            shkronjat.put(tmp, shkronjat.get(tmp)+1);
        }
        return i;
    }

    private void analyze() {
        /*
        Inicializon me 0 variablat: shkronjat, numrat, shenjat
         */
        pergatit();

        for (int i = 0; i < this.gjatesiaTotale; i++) {
            String tmpChar = String.valueOf(this.text.charAt(i));

            // Nese mund te jete grupshkronje kontrollo grupshkronjen
            // dhe kthe indeksin per iterim
            if (i + 1 < this.gjatesiaTotale) {
                int tmp = kontrolloGrupshkronjat(i);
                if(tmp != i){
                    i = tmp;
                    continue;
                }
            }
            if (shkronjat.get(tmpChar) != null) {
                shkronjat.put(tmpChar, shkronjat.get(tmpChar) + 1);
            }
            if (numrat.get(tmpChar) != null) {
                numrat.put(tmpChar, numrat.get(tmpChar) + 1);
            }

            // Per 4 llojet e thonjezave njefishe
            if(tmpChar.equals("’") || tmpChar.equals("'") || tmpChar.equals("‘") || tmpChar.equals("`")){
                extra.put("’", extra.get("’")+1);
            }
            // Per 4 llojet e thonjezave dyfishe
            else if(tmpChar.equals("\"") || tmpChar.equals("„") || tmpChar.equals("“") || tmpChar.equals("”") || tmpChar.equals("“")){
                extra.put("“", extra.get("“")+1);
            }
            else if (extra.get(tmpChar) != null) {
                extra.put(tmpChar, extra.get(tmpChar) + 1);
            } else if (tmpChar.contains("\n")) {
                extra.put("&#13;&#10;", extra.get("&#13;&#10;") + 1);
            }
        }
    }

    private LinkedHashMap<String, Integer> pergatitExtra() {
        extra = new LinkedHashMap<String, Integer>();
        extra.put(" ", 0);
        extra.put("&#13;&#10;", 0);
        extra.put(".", 0);
        extra.put(",", 0);
        extra.put(";", 0);
        extra.put(":", 0);
        extra.put("?", 0);
        extra.put("!", 0);
        extra.put("@", 0);

        extra.put("#", 0);
        extra.put("$", 0);
        extra.put("%", 0);
        extra.put("^", 0);
        extra.put("&", 0);
        extra.put("*", 0);
        extra.put("(", 0);
        extra.put(")", 0);
        extra.put(")", 0);
        extra.put("_", 0);
        extra.put("+", 0);
        extra.put("-", 0);

        /*
        Perdoret per te gjitha thonjezat nje-fishe
         */
        extra.put("’", 0);
        /*
        Perdoret per te gjitha thonjezat 2-fishe
         */
        extra.put("“", 0);

        extra.put("\\", 0);
        extra.put("/", 0);

        return (LinkedHashMap<String, Integer>) extra;
    }

    private LinkedHashMap<String, Integer> pergatitShkronjat() {
        shkronjat = new LinkedHashMap<String, Integer>();
        shkronjat.put("a", 0);
        shkronjat.put("b", 0);
        shkronjat.put("c", 0);
        shkronjat.put("ç", 0);
        shkronjat.put("d", 0);
        shkronjat.put("dh", 0);
        shkronjat.put("e", 0);
        shkronjat.put("ë", 0);
        shkronjat.put("f", 0);
        shkronjat.put("g", 0);
        shkronjat.put("gj", 0);
        shkronjat.put("h", 0);
        shkronjat.put("i", 0);
        shkronjat.put("j", 0);
        shkronjat.put("k", 0);
        shkronjat.put("l", 0);
        shkronjat.put("ll", 0);
        shkronjat.put("m", 0);
        shkronjat.put("n", 0);
        shkronjat.put("nj", 0);
        shkronjat.put("o", 0);
        shkronjat.put("p", 0);
        shkronjat.put("q", 0);
        shkronjat.put("r", 0);
        shkronjat.put("rr", 0);
        shkronjat.put("s", 0);
        shkronjat.put("sh", 0);
        shkronjat.put("t", 0);
        shkronjat.put("th", 0);
        shkronjat.put("u", 0);
        shkronjat.put("v", 0);
        shkronjat.put("x", 0);
        shkronjat.put("xh", 0);
        shkronjat.put("y", 0);
        shkronjat.put("z", 0);
        shkronjat.put("zh", 0);
        return (LinkedHashMap<String, Integer>) shkronjat;
    }

    private LinkedHashMap<String, Integer> pergatitNumrat() {
        numrat = new LinkedHashMap<String, Integer>();
        numrat.put("1", 0);
        numrat.put("2", 0);
        numrat.put("3", 0);
        numrat.put("4", 0);
        numrat.put("5", 0);
        numrat.put("6", 0);
        numrat.put("7", 0);
        numrat.put("8", 0);
        numrat.put("9", 0);
        numrat.put("0", 0);
        return (LinkedHashMap<String, Integer>) numrat;
    }

    public LinkedHashMap<String, LinkedHashMap<String,Integer>> getLidhezat(){
        return this.lidheza.getLidhezat();
    }
    public LinkedHashMap<String, ArrayList<String>> getRrokjet(){
        return this.rrokja.getRrokjet();
    }
    private void pergatit() {
        shkronjat = pergatitShkronjat();
        numrat = pergatitNumrat();
        extra = pergatitExtra();
    }
}
