package com.blendigashi.controller;

import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Blendi Gashi on 4/11/2017.
 */
class Lidheza {
    private String text;
    private Map<String, LinkedHashMap<String, Integer>> lidhezat;

    Lidheza(String t){
        this.text=t;
        /*
        Analiza e lidhezave
         */
        lidhezat = pergatitLidhezat();
        for (String k : lidhezat.keySet()) { // Per secilen kategori te lidhezave
            for (String a : lidhezat.get(k).keySet()) { // Per secilen lidhez
                lidhezat.get(k).put(a, StringUtils.countOccurrencesOf(this.text, a));
            }
        }
    }
    LinkedHashMap<String, LinkedHashMap<String, Integer>> getLidhezat() {
        return (LinkedHashMap<String, LinkedHashMap<String, Integer>>) this.lidhezat;
    }
    private LinkedHashMap<String, LinkedHashMap<String, Integer>> pergatitLidhezat() {
        lidhezat = new LinkedHashMap<String, LinkedHashMap<String, Integer>>();
        lidhezat.put("Shtuese", new LinkedHashMap<String, Integer>() {{
            put(" e ", 0);
            put(" a ", 0);
            put(" dhe ", 0);
            put(" edhe ", 0);
            put(" si edhe ", 0);
        }});
        lidhezat.put("Kundërshtore", new LinkedHashMap<String, Integer>() {{
            put(" po ", 0);
            put(" por ", 0);
            put(" kurse ", 0);
            put(" megjithatë ", 0);
            put(" megjithkëtë ", 0);
            put(" mirëpo ", 0);
            put(" ndërsa ", 0);
            put(" teksa ", 0);
            put(" porse ", 0);
            put(" veç ", 0);
            put(" veçqë ", 0);
            put(" veçse ", 0);
            put(" vetëm ", 0);
            put(" vetëm se ", 0);
        }});
        lidhezat.put("Veçuese", new LinkedHashMap<String, Integer>() {{
            put(" apo ", 0);
            put(" ose ", 0);
        }});
        lidhezat.put("Përmbyllëse", new LinkedHashMap<String, Integer>() {{
            put(" andaj ", 0);
            put(" ndaj ", 0);
            put(" pa ", 0);
            put(" prandaj ", 0);
            put(" domethënë ", 0);
        }});
        lidhezat.put("Ftilluese", new LinkedHashMap<String, Integer>() {{
            put(" se ", 0);
            put(" që ", 0);
            put(" në ", 0);
            put(" nëse ", 0);
        }});
        lidhezat.put("Vendore", new LinkedHashMap<String, Integer>() {{
            put(" ku ", 0);
            put(" tek ", 0);
            put(" nga ", 0);
            put(" kudo ", 0);
            put(" ngado ", 0);
            put(" tekdo ", 0);
            put(" deri ku ", 0);
            put(" gjer ku ", 0);
            put(" nga ku ", 0);
            put(" që ku ", 0);
            put(" kudo që ", 0);
            put(" ngado që ", 0);
            put(" kurdoherë që ", 0);
            put(" para se ", 0);
            put(" posa që ", 0);
            put(" që kur ", 0);
            put(" që se ", 0);
            put(" qysh se ", 0);
            put(" sapo që ", 0);
            put(" sa herë që ", 0);
            put(" sa kohë që ", 0);
            put(" përpara se ", 0);
        }});
        lidhezat.put("Shkakore", new LinkedHashMap<String, Integer>() {{
            put(" se ", 0);
            put(" sepse ", 0);
            put(" si ", 0);
            put(" pasi ", 0);
            put(" mbasi ", 0);
            put(" derisa ", 0);
            put(" gjersa ", 0);
            put(" përderisa ", 0);
            put(" kur ", 0);
            put(" që ", 0);
            put(" sapo ", 0);
            put(" meqenëse ", 0);
            put(" meqë ", 0);
            put(" ngaqë ", 0);
            put(" ngase ", 0);
            put(" duke qenë se ", 0);
            put(" nga frika se ", 0);
            put(" nga shkaku që ", 0);
            put(" për arsye se ", 0);
            put(" për shkak se ", 0);
            put(" posa që ", 0);
            put(" sapo që ", 0);

        }});
        lidhezat.put("Qëllimore", new LinkedHashMap<String, Integer>() {{
            put(" me qëllim që ", 0);
            put(" në mënyrë që ", 0);
            put(" me të vetmin qëllim që ", 0);
        }});
        lidhezat.put("Krahasore", new LinkedHashMap<String, Integer>() {{
            put(" sa ", 0);
            put(" aq sa ", 0);
            put(" se ", 0);
            put(" sesa ", 0);
            put(" se ç'", 0);
            put(" nga ç", 0);
            put(" nga sa ", 0);
        }});
        lidhezat.put("Mënyrore", new LinkedHashMap<String, Integer>() {{
            put(" si ", 0);
            put(" siç ", 0);
            put(" ashtu si ", 0);
            put(" ashtu siç ", 0);
            put(" po si ", 0);
            put(" por si ", 0);
            put(" sikurse ", 0);
            put(" sikundërse ", 0);
        }});
        lidhezat.put("Kushtore", new LinkedHashMap<String, Integer>() {{
            put(" në ", 0);
            put(" po ", 0);
            put(" nëse ", 0);
            put(" kur ", 0);
            put(" sikur ", 0);
            put(" në qoftë se ", 0);
            put(" në është se ", 0);
            put(" në rast se ", 0);
            put(" po qe se ", 0);
            put(" me kusht që ", 0);
        }});
        lidhezat.put("Rrjedhimore", new LinkedHashMap<String, Integer>() {{
            put(" saqë ", 0);
            put(" aq sa ", 0);
            put(" kështu që ", 0);
        }});
        lidhezat.put("Lejore", new LinkedHashMap<String, Integer>() {{
            put(" megjithëse ", 0);
            put(" megjithëqe ", 0);
            put(" ndonëse ", 0);
            put(" sado ", 0);
            put(" sido ", 0);
            put(" edhe në ", 0);
            put(" edhe në qoftë se ", 0);
            put(" edhe po ", 0);
            put(" edhe pse ", 0);
            put(" edhe sepse ", 0);
            put(" edhe sikur ", 0);
        }});

        return (LinkedHashMap<String, LinkedHashMap<String, Integer>>) lidhezat;
    }

}
