package com.blendigashi.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Blendi Gashi on 4/11/2017.
 */
public class GjuhaShqipe {
    private static final HashSet<Character> zanoret = new HashSet<Character>();
    private static final HashSet<String> diftongjet = new HashSet<String>();
    private static final Map<String, ArrayList<String>> customWords = new LinkedHashMap<String, ArrayList<String>>();

    static {
        customWords.put("mithat", new ArrayList<String>(){{add("mit"); add("hat");}});
        customWords.put("shtathedhur", new ArrayList<String>(){{add("shtat"); add("he"); add("dhur");}});
    }
    static {
        zanoret.add('a');
        zanoret.add('e');
        zanoret.add('ë');
        zanoret.add('i');
        zanoret.add('o');
        zanoret.add('u');
        zanoret.add('y');
    }
    static {
        diftongjet.add("dh");
        diftongjet.add("gj");
        diftongjet.add("ll");
        diftongjet.add("nj");
        diftongjet.add("rr");
        diftongjet.add("sh");
        diftongjet.add("th");
        diftongjet.add("xh");
        diftongjet.add("zh");
    }
    boolean eshteZanore(Character c) {
        return zanoret.contains(c);
    }
    boolean eshteDiftongje(String c){
        return diftongjet.contains(c);
    }
    /*
    Kthen vlere nese fjala eshte komplekse
     */
    boolean eshteFjaleTjeter(String c){
        return customWords.containsKey(c);
    }
    ArrayList<String> merrRrokjet (String c){
        return customWords.getOrDefault(c, new ArrayList<String>());
    }
}
